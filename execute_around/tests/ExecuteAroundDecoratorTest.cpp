/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#define CATCH_CONFIG_MAIN // This tells Catch to provide a main()
#include "catch2/catch.hpp"

#include "slci/toolbox/ea/ExecuteAroundDecorator.hpp"

using namespace std::chrono_literals;

std::vector<std::string> callRegister;

struct TestClass
{
    void method()
    {
        callRegister.push_back("TestClass::method()");
    }

    void method() const
    {
        callRegister.push_back("TestClass::method() const");
    }
};


SCENARIO("Objects wrapped with ExecuteAroundDecorator can be accessed with operator->()", "[ExecuteAroundDecorator]")
{
    GIVEN("Any object")
    {
        auto testObj = TestClass{};
        callRegister.clear();

        WHEN("the object is wrapped with ExecuteAroundDecorator")
        {
            auto testWrapper = slci::toolbox::ea::ExecuteAroundDecorator{nullptr, nullptr, testObj};

            THEN("the wrapped object can be accessed trough operator->()")
            {
                REQUIRE(bool(testWrapper) == true);

                testWrapper->method();

                REQUIRE(callRegister.size() == 1ul);
                REQUIRE(callRegister.at(0).compare("TestClass::method()") == 0);
            }
        }
    }

    GIVEN("Any const object")
    {
        const auto testObj = TestClass{};
        callRegister.clear();

        WHEN("the const object is wrapped with ExecuteAroundDecorator")
        {
            auto testWrapper = slci::toolbox::ea::ExecuteAroundDecorator{nullptr, nullptr, testObj};

            THEN("the wrapped const object can be accessed trough operator->()")
            {
                REQUIRE(bool(testWrapper) == true);

                testWrapper->method();

                REQUIRE(callRegister.size() == 1ul);
                REQUIRE(callRegister.at(0).compare("TestClass::method() const") == 0);
            }
        }
    }
}


SCENARIO("Calling wrapped object's method is preceded with call to user defined callback", "[ExecuteAroundDecorator]")
{
    GIVEN("A wrapped object")
    {
        auto testObj = TestClass{};
        callRegister.clear();

        auto preCallback = [] {
            callRegister.push_back("preCallback()");
        };

        auto testWrapper = slci::toolbox::ea::ExecuteAroundDecorator{preCallback, nullptr, testObj};

        WHEN("The object's method is called")
        {
            testWrapper->method();

            THEN("User defined callback is executed before the actual call to objects method")
            {
                REQUIRE(callRegister.size() == 2ul);
                REQUIRE(callRegister.at(0).compare("preCallback()") == 0);
                REQUIRE(callRegister.at(1).compare("TestClass::method()") == 0);
            }
        }
    }
}


SCENARIO("After calling wrapped object's method a user defined callback is executed", "[ExecuteAroundDecorator]")
{
    GIVEN("A wrapped object")
    {
        auto testObj = TestClass{};
        callRegister.clear();

        auto postCallback = [](const int64_t) {
            callRegister.push_back("postCallback()");
        };

        auto testWrapper = slci::toolbox::ea::ExecuteAroundDecorator{nullptr, postCallback, testObj};

        WHEN("The object's method is called")
        {
            testWrapper->method();

            THEN("User defined callback is executed after the call to objects methosd")
            {
                REQUIRE(callRegister.size() == 2ul);
                REQUIRE(callRegister.at(0).compare("TestClass::method()") == 0);
                REQUIRE(callRegister.at(1).compare("postCallback()") == 0);
            }
        }
    }
}


struct FakeClock
{
    using time_point = std::chrono::high_resolution_clock::time_point;

    static std::vector<std::chrono::nanoseconds> returnClockValues;
    static std::vector<std::chrono::nanoseconds>::const_iterator nextReturnValue;

    static void setRetValues(std::vector<std::chrono::nanoseconds> values)
    {
        std::swap(returnClockValues, values);
        nextReturnValue = returnClockValues.cbegin();
    }

    static time_point now()
    {
        const auto ret_time_point = time_point{*nextReturnValue};
        nextReturnValue++;
        return ret_time_point;
    }
};
std::vector<std::chrono::nanoseconds> FakeClock::returnClockValues{};
std::vector<std::chrono::nanoseconds>::const_iterator FakeClock::nextReturnValue;


SCENARIO("A user defined callback passes the time in seconds measured since the call to object's method till the return from it", "[ExecuteAroundDecorator]")
{
    GIVEN("A wrapped object")
    {
        auto testObj = TestClass{};

        FakeClock::setRetValues({12345ns, 14482ns});

        int64_t nano_secs = 0ul;
        auto postCallback = [&nano_secs](const int64_t ns) {
            nano_secs = ns;
        };

        auto testWrapper = slci::toolbox::ea::ExecuteAroundDecorator<TestClass, FakeClock>{nullptr, postCallback, testObj};

        WHEN("The object's method is called")
        {
            testWrapper->method();

            THEN("The ExecuteAroundDecorator passes time value in nanoseconds to user defined callback")
            {
                REQUIRE(nano_secs == 2137ul);
            }
        }
    }
}