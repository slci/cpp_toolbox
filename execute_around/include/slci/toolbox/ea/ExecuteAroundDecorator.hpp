/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2019 Sławomir Cielepak <slawomir.cielepak@gmail.com>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SLCI_TOOLBOX_EA_EXECUTEAROUNDDECORATOR_HPP
#define SLCI_TOOLBOX_EA_EXECUTEAROUNDDECORATOR_HPP

#include <chrono>

namespace slci::toolbox::ea
{

/**
 * @brief ExecuteAroundDecorator - a wrapper around any object that executes additional routines around calls to object's methods
 * 
 * @tparam TWrapped
 * @tparam TClock
 * 
 * Example usage:
 * @code
 *    struct Subject
 *    {
 *        void testCall() {}
 *    }
 *    ...
 *    auto sbj = Subject{};
 * 
 *    auto pre = [](const std::string& method) {
 *        std::cout << "Calling testCall()\n";
 *    };
 *    auto post = [](const std::string& method, const int64_t nano_seconds) {
 *        std::cout << "Call to testCall() finished in " << nano_seconds << " ns\n";
 *    };
 * 
 *    auto traceableSubject = ExecuteAroundDecorator<Subject>{pre, post, sbj};
 * 
 *    // Will print "Calling testCall()", then call Subject::testCall() and then print "Call to testCall() finished in X s"
 *    traceableSubject->testCall();
 * @endcode
 */
template <
    typename TWrapped,
    typename TClock = std::chrono::high_resolution_clock>
class ExecuteAroundDecorator
{
public:
    template <typename TExecAround>
    class Proxy;

    using Wrapped = TWrapped;
    using Clock = TClock;
    using PreCallback = std::function<void()>;
    using PostCallback = std::function<void(const int64_t)>;

    /**
     * @brief Constructor wrapping std::shared_ptr<TWrapped>
     * 
     * @param pre    Callable that will be called before dereference of wrapped object
     * @param post   Callable that will be called after dereference of wrapped object
     * @param obj    Reference to the wrapped object
     */
    explicit ExecuteAroundDecorator(PreCallback pre, PostCallback post, TWrapped &obj)
        : ExecuteAroundDecorator(pre, post, &obj)
    {
    }

    /**
     * @brief Constructor wrapping std::shared_ptr<TWrapped>
     * 
     * @param pre    Callable that will be called before dereference of wrapped object
     * @param post   Callable that will be called after dereference of wrapped object
     * @param obj    Pointer to the wrapped object
     */
    explicit ExecuteAroundDecorator(PreCallback pre, PostCallback post, TWrapped *obj)
        : m_WrappedObject(obj)
    {
        m_Pre = pre;
        m_Post = post;
    }

    /**
     * @brief Destroy the ExecuteAroundDecorator object
     */
    ~ExecuteAroundDecorator()
    {
    }

    /**
     * @brief Returns whether wprapped pointer holds a valid object
     * 
     * @return true if wrapped object is valid, false otherwise
     */
    operator bool() const
    {
        return (m_WrappedObject != nullptr);
    }

    /**
     * @brief 
     * 
     * @return Proxy<ExecuteAroundDecorator> 
     */
    Proxy<ExecuteAroundDecorator>
    operator->() const
    {
        return Proxy<ExecuteAroundDecorator>(m_Pre, m_Post, m_WrappedObject);
    }

    /**
     * @brief Proxy to the wrapped object that implements the execute-around pattern
     * 
     * @tparam TExecAround 
     */
    template <typename TExecAround>
    class Proxy
    {
        friend class ExecuteAroundDecorator;

    private:
        /**
         * @brief The Proxy constructor, made private to allow construction by ExecuteAroundDecorator only
         * 
         * @param pre    Callable that will be called before dereference of wrapped object
         * @param post   Callable that will be called after dereference of wrapped object
         * @param obj    Pointer to the wrapped object
         */
        explicit Proxy(typename TExecAround::PreCallback pre, typename TExecAround::PostCallback post, typename TExecAround::Wrapped *obj) : m_Post(post), m_WrappedObject(obj)
        {
            m_StartTimePoint = TExecAround::Clock::now();

            if (pre)
            {
                pre();
            }
        }

        /**
         * @brief Deleted copy constructor
         */
        Proxy(const Proxy &) = delete;

        /**
         * @brief Deleted copy assignment operator
         */
        Proxy &
        operator=(const Proxy &) = delete;

        /**
         * @brief Deleted move constructor
         */
        Proxy(Proxy &&) = delete;

        /**
         * @brief Deleted move assignment operator
         */
        Proxy &
        operator=(Proxy &&) = delete;

    public:
        /**
         * @brief Proxy destructor
         * 
         * Executes the stored post operation, passing the time (in nanoseconds) since the Proxy creation.
         */
        ~Proxy()
        {
            if (m_Post)
            {
                const auto endTimePoint = TExecAround::Clock::now();
                const auto nano_secs = std::chrono::duration_cast<std::chrono::nanoseconds>(endTimePoint - m_StartTimePoint).count();

                try
                {
                    m_Post(nano_secs);
                }
                catch (...)
                {
                    // Supress any potential exception from destructor
                }
            }
        }

        /**
         * @brief  Accessor of the wrapped object
         * 
         * @return Pointer to the wrapped object
         */
        typename TExecAround::Wrapped *operator->() const
        {
            return m_WrappedObject;
        }

    protected:
        typename TExecAround::PostCallback m_Post;
        typename TExecAround::Wrapped *m_WrappedObject;
        typename TExecAround::Clock::time_point m_StartTimePoint;
    };

    PreCallback m_Pre;
    PostCallback m_Post;
    TWrapped *m_WrappedObject;
};
} // namespace slci::toolbox::ea

#endif /* SLCI_TOOLBOX_EA_EXECUTEAROUNDDECORATOR_HPP */
